(function ($) {
  Drupal.behaviors.shortlist = {
    attach: function (context, settings) {
      shortlistItem();
      removeItemShortlist();

      // Shortlist an item.
      function shortlistItem() {
        $(document).delegate(".shortlist-add", "click", function() {
          var data = JSON.parse($.cookie('shortlisted'));
          var entityType = $(this).attr('entity-type');
          var id = $(this).attr('id');

          if (data === null) {
            data = {};
            data.node = [];
          }

          data.node.push(id);

          var date = new Date();
          date.setTime(date.getTime() + (Drupal.settings.shortlist.expirationInterval * 60 * 1000));

          $.cookie("shortlisted", JSON.stringify(data), {expires: date, path: '/'});
          $(this).removeClass('shortlist-add');
          $(this).addClass('shortlist-remove');
          $(this).html('<a>' + Drupal.t('Remove') + '</a>');
        });
      }

      // Remove from shortlist.
      function removeItemShortlist() {
        $(document).delegate(".shortlist-remove", "click", function() {
          var data = JSON.parse($.cookie("shortlisted"));
          var id = $(this).attr('id');

          data.node = $.grep(data.node, function(value) {
            return value != id;
          });

          var date = new Date();
          date.setTime(date.getTime() + (Drupal.settings.shortlist.expirationInterval * 60 * 1000));

          $.cookie("shortlisted", JSON.stringify(data), {expires: date, path: '/'});
          $(this).removeClass('shortlist-remove');
          $(this).addClass('shortlist-add');
          $(this).html('<a>' + Drupal.t('Shortlist') + '</a>');
        });
      }
    }
  };
})(jQuery);
