<?php

/**
 * @file
 * Page callbacks.
 */

/**
 * Render shortlisted items page.
 *
 * @return string
 *   HTML for rendering shortlisted items page.
 */
function shortlist_shortlisted_items_page() {
  $query_params = drupal_get_query_parameters();

  if (!empty($query_params['node'])) {
    $nids = explode(',', $query_params['node']);
    $nodes = node_load_multiple($nids);

    foreach ($nodes as $node) {
      $items[] = array(
        'data' => $node->title,
      );
    }

    return theme('item_list', array(
      'items' => $items,
    ));
  }
  else {
    return t('No shortlisted items');
  }
}
