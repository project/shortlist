<?php

/**
 * Admin pages.
 */

function shortlist_general_settings($form, &$form_state) {
  $form = array();
  $content_types = node_type_get_types();

  foreach ($content_types as $key => $type) {
    $options[$key] = $type->name;
  }

  $form['shortlist_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#description' => t('Content types where do you want to show shortlist widget'),
    '#options' => $options,
    '#default_value' => variable_get('shortlist_node_types', array()),
  );

  return system_settings_form($form);
}
